#+TITLE: ANDREW'S GNU EMACS CONFIG
#+AUTHOR: ANDREW CARPIN

* TABLE OF CONTEMTS :toc:
- [[#important-put-this-in-your-initel][IMPORTANT! PUT THIS IN YOUR INIT.EL]]
  - [[#initel][INIT.EL]]
- [[#about-this-config][ABOUT THIS CONFIG]]
- [[#programs-to-load-first][PROGRAMS TO LOAD FIRST]]
  - [[#setup-packageel-to-work-with-melpa][Setup Package.el to Work with MELPA]]
  - [[#use-package][Use-Package]]
  - [[#evil-mode][Evil Mode]]
- [[#fonts][FONTS]]
  - [[#setting-the-font-face][Setting the Font Face]]
  - [[#zooming-in-and-out][Zooming In and Out]]
- [[#gui-settings][GUI SETTINGS]]
  - [[#tweaks-to-make-the-ui-more-minimal][Tweaks to make the UI more minimal]]
  - [[#display-line-numbers-and-truncated-lines][Display Line Numbers and Truncated Lines]]
  - [[#doom-modeline][Doom Modeline]]
- [[#ivy-counselswiper][IVY (COUNSEL/SWIPER]]
  - [[#installing-ivy-and-basic-setup][Installing Ivy And Basic Setup]]
  - [[#making-m-x-great-again][Making M-x Great Again!]]
  - [[#ivy-postframe][Ivy-Postframe]]
- [[#magit][MAGIT]]
- [[#org-mode][ORG MODE]]
  - [[#defining-a-few-things][Defining a few things]]
  - [[#enabling-org-bullets][Enabling Org Bullets]]
  - [[#source-code-block-tag-expansion][Source Code Block Tag Expansion]]
  - [[#source-code-block-syntax-highlighting][Source Code Block Syntax Highlighting]]
  - [[#automatically-create-table-of-contents][Automatically Create Table of Contents]]
- [[#theme][THEME]]
- [[#which-key][WHICH-KEY]]

* IMPORTANT! PUT THIS IN YOUR INIT.EL
  Even if you do not want to config your Emacs with an init.el file the following code will tell init.el to read the source code blocks from this file!
  
** INIT.EL
   (org-babel-load-file
    (expand-file-name
     "config.org"
     user-emacs-directory))
     
* ABOUT THIS CONFIG
  This is my attempt at a GNU Emacs config. I have been a Doom Emacs user for some time and with the help and expertise of Derek from Distrotube and David from System Crafters I would like to try to develope my own GNU Emacs config. Anyone is welcome to use this configuration and alter it to suit their own needs!
  
* PROGRAMS TO LOAD FIRST
  The order in which the various Emacs modules load is very important.  So the very first code block is going to contain essential modules that many other modules will depend on later in this config.
  
** Setup Package.el to Work with MELPA
   #+begin_src emacs-lisp
     (require 'package)
     (add-to-list 'package-archives
		  '("melpa" . "https://melpa.org/packages/"))
     (package-refresh-contents)
     (package-initialize)

   #+end_src

** Use-Package
   Install use-package and enable ensure t globally
   #+begin_src emacs-lisp
     (unless (package-installed-p 'use-package)
       (package-install 'use-package))
     (require 'use-package)
     (setq use-package-always-ensure t)

   #+end_src

** Evil Mode
   Evil is an extensible ‘vi’ layer for Emacs. It emulates the main features of Vim, and provides facilities for writing custom extensions.  Evil Collection is also installed since it adds ‘evil’ bindings to parts of Emacs that the standard Evil package does not cover, such as: calenda, help-mode and ibuffer.

   #+begin_src emacs-lisp
   (use-package evil
     :init      ;; tweak evil's configuration before loading it
     (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
     (setq evil-want-keybinding nil)
     (setq evil-vsplit-window-right t)
     (setq evil-split-window-below t)
     (evil-mode))
   (use-package evil-collection
     :after evil
     :config
     (setq evil-collection-mode-list '(dashboard dired ibuffer))
     (evil-collection-init))
   (use-package evil-tutor)

   #+end_src

* FONTS
  Defining fonts we want to use is our Emacs config

** Setting the Font Face

  #+begin_src emacs-lisp
    (set-face-attribute 'default nil
      :font "SauceCodePro Nerd Font"
      :height 110)
    (set-face-attribute 'variable-pitch nil
      :font "Ubuntu Nerd Font"
      :height 120)
    (set-face-attribute 'fixed-pitch nil
      :font "SauceCodePro Nerd Font"
      :height 110)

    ;; Needed if using emacsclient. Otherwise, your fonts will be smaller than expected.
    (add-to-list 'default-frame-alist '(font . "SauceCodePro Nerd Font-11"))
    ;; Changes certain keywords to symbols, such as lamda!
    (setq global-prettify-symbols-mode t)

  #+end_src

** Zooming In and Out
   You can use the bindings CTRL plus =/- for zooming in/out.  You can also use CTRL plus the mouse wheel for zooming in/out.

  #+begin_src emacs-lisp
    ;; zoom in/out like we do everywhere else.
    (global-set-key (kbd "C-=") 'text-scale-increase)
    (global-set-key (kbd "C--") 'text-scale-decrease)
 
  #+end_src

* GUI SETTINGS
  
** Tweaks to make the UI more minimal

  #+begin_src emacs-lisp
    (setq inhibit-startup-message t)
    (scroll-bar-mode -1)
    (tool-bar-mode -1)
    (tooltip-mode -1)
    (menu-bar-mode -1)
  #+end_src
  
** Display Line Numbers and Truncated Lines
 
  #+begin_src emacs-lisp
    (global-display-line-numbers-mode 1)
    (global-visual-line-mode t)

  #+end_src

** Doom Modeline

  #+begin_src emacs-lisp
    (use-package doom-modeline)
    (doom-modeline-mode 1)

  #+end_src
   
* IVY (COUNSEL/SWIPER
  Ivy, counsel and swiper are a generic completion mechanism for Emacs.  Ivy-rich allows us to add descriptions alongside the commands in M-x.

** Installing Ivy And Basic Setup

  #+begin_src emacs-lisp
    (use-package counsel
      :after ivy
      :config (counsel-mode))
    (use-package ivy
      :defer 0.1
      :diminish
      :bind
      (("C-c C-r" . ivy-resume)
       ("C-x B" . ivy-switch-buffer-other-window))
      :custom
      (setq ivy-count-format "(%d/%d) ")
      (setq ivy-use-virtual-buffers t)
      (setq enable-recursive-minibuffers t)
      :config
      (ivy-mode))
    (use-package ivy-rich
      :after ivy
      :custom
      (ivy-virtual-abbreviate 'full
       ivy-rich-switch-buffer-align-virtual-buffer t
       ivy-rich-path-style 'abbrev)
      :config
      (ivy-set-display-transformer 'ivy-switch-buffer
				   'ivy-rich-switch-buffer-transformer)
      (ivy-rich-mode 1)) ;; this gets us descriptions in M-x.
    (use-package swiper
      :after ivy
      :bind (("C-s" . swiper)
	     ("C-r" . swiper)))
  
  #+end_src

** Making M-x Great Again!
   The following line removes the annoying ‘^’ in things like counsel-M-x and other ivy/counsel prompts.  The default ‘^’ string means that if you type something immediately after this string only completion candidates that begin with what you typed are shown.  Most of the time, I’m searching for a command without knowing what it begins with though.

  #+begin_src emacs-lisp
    (setq ivy-initial-inputs-alist nil)

  #+end_src

  Smex is a package the makes M-x remember our history.  Now M-x will show our last used commands first.

  #+begin_src emacs-lisp
    (use-package smex)
    (smex-initialize)

  #+end_src
  
** Ivy-Postframe

  #+begin_src emacs-lisp
    (use-package ivy-posframe
      :init
      (setq ivy-posframe-display-functions-alist
	'((swiper                     . ivy-posframe-display-at-point)
	  (complete-symbol            . ivy-posframe-display-at-point)
	  (counsel-M-x                . ivy-display-function-fallback)
	  (counsel-esh-history        . ivy-posframe-display-at-window-center)
	  (counsel-describe-function  . ivy-display-function-fallback)
	  (counsel-describe-variable  . ivy-display-function-fallback)
	  (counsel-find-file          . ivy-display-function-fallback)
	  (counsel-recentf            . ivy-display-function-fallback)
	  (counsel-register           . ivy-posframe-display-at-frame-bottom-window-center)
	  (dmenu                      . ivy-posframe-display-at-frame-top-center)
	  (nil                        . ivy-posframe-display))
	ivy-posframe-height-alist
	'((swiper . 20)
	  (dmenu . 20)
	  (t . 10)))
      :config
      (ivy-posframe-mode 1)) ; 1 enables posframe-mode, 0 disables it.

  #+end_src

* MAGIT
  A git client for emacs.
  
  #+begin_src emacs-lisp
  (use-package magit)

  #+end_src
  
* ORG MODE
  Definately one of the greatest features emacs has to offer!

** Defining a few things
  #+begin_src emacs-lisp
    (add-hook 'org-mode-hook 'org-indent-mode)
    (setq org-directory "~/Org/"
	  org-agenda-files '("~/Org/agenda.org")
	  org-default-notes-file (expand-file-name "notes.org" org-directory)
	  org-ellipsis " ▼ "
	  org-log-done 'time
	  org-journal-dir "~/Org/journal/"
	  org-journal-date-format "%B %d, %Y (%A) "
	  org-journal-file-format "%Y-%m-%d.org"
	  org-hide-emphasis-markers t)
    (setq org-src-preserve-indentation nil
	  org-src-tab-acts-natively t
	  org-edit-src-content-indentation 0)

  #+end_src

** Enabling Org Bullets
   Using bullets rather than asterisks
   #+begin_src emacs-lisp
   (use-package org-bullets)
   (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

   #+end_src

** Source Code Block Tag Expansion
Org-tempo is a package that allows for ‘<s’ followed by TAB to expand to a begin_src tag.  Other expansions available include:

| Typing the below + TAB | Expands to ...                          |
|------------------------+-----------------------------------------|
| <a                     | ’#+BEGIN_EXPORT ascii’ … ‘#+END_EXPORT  |
|------------------------+-----------------------------------------|
| <c                     | ’#+BEGIN_CENTER’ … ‘#+END_CENTER’       |
|------------------------+-----------------------------------------|
| <C                     | ’#+BEGIN_COMMENT’ … ‘#+END_COMMENT’     |
|------------------------+-----------------------------------------|
| <e                     | ’#+BEGIN_EXAMPLE’ … ‘#+END_EXAMPLE’     |
|------------------------+-----------------------------------------|
| <E                     | ’#+BEGIN_EXPORT’ … ‘#+END_EXPORT’       |
|------------------------+-----------------------------------------|
| <h                     | ’#+BEGIN_EXPORT html’ … ‘#+END_EXPORT’  |
|------------------------+-----------------------------------------|
| <l                     | ’#+BEGIN_EXPORT latex’ … ‘#+END_EXPORT’ |
|------------------------+-----------------------------------------|
| <q                     | ’#+BEGIN_QUOTE’ … ‘#+END_QUOTE’         |
|------------------------+-----------------------------------------|
| <s                     | ’#+BEGIN_SRC’ … ‘#+END_SRC’             |
|------------------------+-----------------------------------------|
| <v                     | ’#+BEGIN_VERSE’ … ‘#+END_VERSE’         |
|------------------------+-----------------------------------------|
                                                     
#+begin_src emacs-lisp
(use-package org-tempo
  :ensure nil) ;; tell use-package not to try to install org-tempo since it's already there.  

#+end_src

** Source Code Block Syntax Highlighting
We want the same syntax highlighting in source blocks as in the native language files.
#+begin_src emacs-lisp
(setq org-src-fontify-natively t
    org-src-tab-acts-natively t
    org-confirm-babel-evaluate nil
    org-edit-src-content-indentation 0)

#+end_src

** Automatically Create Table of Contents
Toc-org helps you to have an up-to-date table of contents in org files without exporting (useful useful for README files on GitHub).  Use :TOC: to create the table.

#+begin_src emacs-lisp
(use-package toc-org
  :commands toc-org-enable
  :init (add-hook 'org-mode-hook 'toc-org-enable))

#+end_src

* THEME
  We need a nice colorscheme.  The Doom Emacs guys have a nice collection of themes, so let’s install them!

  #+begin_src emacs-lisp
    (use-package doom-themes
      :ensure t)  
      (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
	    doom-themes-enable-italic t) ; if nil, italics is universally disabled
      (load-theme 'doom-one t)

  #+end_src
  
* WHICH-KEY
  Which-key is a minor mode for Emacs that displays the key bindings following your currently entered incomplete command (a prefix) in a popup.

  #+begin_src emacs-lisp
    (use-package which-key
      :init
      (setq which-key-side-window-location 'bottom
	    which-key-sort-order #'which-key-key-order-alpha
	    which-key-sort-uppercase-first nil
	    which-key-add-column-padding 1
	    which-key-max-display-columns nil
	    which-key-min-display-lines 6
	    which-key-side-window-slot -10
	    which-key-side-window-max-height 0.25
	    which-key-idle-delay 0.8
	    which-key-max-description-length 25
	    which-key-allow-imprecise-window-fit t
	    which-key-separator " → " ))
    (which-key-mode)
  
  #+end_src
